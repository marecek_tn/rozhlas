//
//  AppDelegate.swift
//  RocketLauncher
//
//  Created by Marek on 14/02/2020.
//  Copyright © 2020 Marek Chrenko. All rights reserved.
//

import Cocoa

@NSApplicationMain
class RocketLauncherDelegate: NSObject, NSApplicationDelegate {

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        
        let runningApps = NSWorkspace.shared.runningApplications
        let isRunning = runningApps.contains {
            $0.bundleIdentifier == "co.chren.macos.Rozhlas"
        }

        if !isRunning {
            var path = Bundle.main.bundlePath as NSString
            for _ in 1...4 {
                path = path.deletingLastPathComponent as NSString
            }
            NSWorkspace.shared.launchApplication(path as String)
        } 
    }

}

