//
//  StationManager.swift
//  Rozhlas
//
//  Created by Marek on 03/02/2020.
//  Copyright © 2020 Marek Chrenko. All rights reserved.
//

import Foundation
import Combine
import MediaPlayer

// Takes care of Player and Playlist
class StationManager {
    
    private var currentStation: Station?
    private var player = Player()
    private var playlist = PlaylistManager()
    
    func play(station: Station) {
        stop()
        self.currentStation = station
        player.play(station: station)
        playlist.startFetching(forStation: station)
        
        DispatchQueue.main.async {
            let nowPlaying: [String: Any] = [
                MPMediaItemPropertyTitle: station.name,
                MPMediaItemPropertyArtist: NSLocalizedString("playlist.not.available", comment: "Playlist not available")
            ]
            MPNowPlayingInfoCenter.default().playbackState = .playing
            MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlaying
        }
        
        NotificationCenter.default.post(name: .streamStart, object: self.currentStation)
    }
    
    func stop() {
        NotificationCenter.default.post(name: .streamEnd, object: self.currentStation)
        self.player.pause()
        self.playlist.stopFetching()
        
        MPNowPlayingInfoCenter.default().playbackState = .stopped
        
        if let currentStation = currentStation {
            MPNowPlayingInfoCenter.default().nowPlayingInfo = [
                MPMediaItemPropertyTitle: "\(currentStation.name)",
                MPMediaItemPropertyArtist: NSLocalizedString("touchbar.tap.to.play", comment: "Tap to play")
            ]
        }
    }
    
    func toggle(station: Station) {
        isAlreadyPlaying(station: station) ? stop() : play(station: station)
    }
    
    func toggle() {
        if let currentStation = currentStation {
            toggle(station: currentStation)
        }
    }
    
    private func isAlreadyPlaying(station: Station) -> Bool {
        return self.currentStation == station && player.isPlaying
    }
}
