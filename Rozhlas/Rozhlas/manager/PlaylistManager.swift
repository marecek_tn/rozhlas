//
//  PlaylistManager.swift
//  Rozhlas
//
//  Created by Marek on 01/02/2020.
//  Copyright © 2020 Marek Chrenko. All rights reserved.
//

import Foundation
import Combine
import MediaPlayer
import AppKit

extension Notification.Name {
    static let currentSong = Notification.Name("currentSong")
    static let streamStart = Notification.Name("streamStart")
    static let streamEnd = Notification.Name("streamEnd")
}

class PlaylistManager {
    
    private static let PLAYLIST_FETCH_DELAY = 30.0 // in seconds
    
    private(set) var currentSong: Song? {
        didSet {
            NotificationCenter.default.post(name: .currentSong, object: currentSong)
            // notify MPNowPlayingInfoCenter
            notifySystem(song: currentSong)
        }
    }
    
    func notifySystem(song: Song?) {
        guard let song = song else {
            return
        }
        
        DispatchQueue.main.async {
            var nowPlaying: [String: Any] = [
                MPMediaItemPropertyTitle: song.track,
                MPMediaItemPropertyArtist: song.interpret
            ]
            
            if let image = NSImage.init(named: "AppIcon") {
                nowPlaying[MPMediaItemPropertyArtwork] = MPMediaItemArtwork(boundsSize: image.size) { _ in image }
            }
            MPNowPlayingInfoCenter.default().playbackState = .playing
            MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlaying
        }
    }
    
    private var networkCancellable: AnyCancellable?
    private var dispatchWorkItem: DispatchWorkItem?
    
    func startFetching(forStation station: Station, delay: Double = 0.0) {
        if station.playlistUrl == nil {
            self.currentSong = nil
            return
        }
        
        self.dispatchWorkItem?.cancel()
        
        self.dispatchWorkItem = DispatchWorkItem { [weak self] in
            self?.fetchSong(forStation: station)
            self?.startFetching(forStation: station, delay: PlaylistManager.PLAYLIST_FETCH_DELAY)
        }
        
        if let dwi = dispatchWorkItem {
            DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: dwi)
        }
    }
    
    func stopFetching() {
        self.currentSong = nil
        dispatchWorkItem?.cancel()
        networkCancellable?.cancel()
    }
    
    func fetchSong(forStation station: Station) {
        guard let playlistUrl = station.playlistUrl else {
            return
        }
        
        if CZECH_STATIONS.contains(station) {
            loadCzechPlaylist(url: playlistUrl)
        } else {
            loadSlovakPlaylist(url:playlistUrl)
        }
    }
}

extension PlaylistManager {
    
    func loadCzechPlaylist(url: URL) {
        self.networkCancellable?.cancel()
        
        self.networkCancellable = URLSession.shared.dataTaskPublisher(for: url)
            .map({ data, response in
                return data
            })
            .decode(type: CzechPlaylist?.self, decoder: JSONDecoder())
            .map(czechPlaylistToSong(_:))
            .replaceError(with: nil)
            .eraseToAnyPublisher()
            .sink(receiveValue: { [weak self] song in
                self?.currentSong = song
            })
    }
    
    func loadSlovakPlaylist(url: URL) {
        
        self.networkCancellable?.cancel()
        
        self.networkCancellable = URLSession.shared.dataTaskPublisher(for: url)
            .map({ data, response in
                return data
            })
            .map(slovakPlaylistToSong(_:))
            .replaceError(with: nil)
            .eraseToAnyPublisher()
            .sink(receiveValue: { [weak self] song in
                self?.currentSong = song
            })
    }
    
    func czechPlaylistToSong(_ playlist: CzechPlaylist?) -> Song? {
        if let pl = playlist {
            return pl.playlist.last!
        }
        return nil
    }
    
    func slovakPlaylistToSong(_ data: Data) -> Song? {
        guard let htmlString = String.init(data: data, encoding: .utf8) else { return nil }
        guard let snippetPlaylistRange = htmlString.range(of: "snippet-playlist")?.upperBound... else { return nil }
        let snippetPlaylist = String(htmlString[snippetPlaylistRange])

        // find programmeTime">
        guard let programmeTimeRange = snippetPlaylist.range(of: "programmeTime\">")?.upperBound,
            let programmeTimeRangeEnd = snippetPlaylist.range(of: "</span>")?.lowerBound else { return nil}
        let programmeTime = String(snippetPlaylist[programmeTimeRange..<programmeTimeRangeEnd])
        
        // find title">
        guard let titleRange = snippetPlaylist.range(of: "title\">")?.upperBound,
            let titleRangeEnd = snippetPlaylist.range(of: "</span>", range: titleRange..<snippetPlaylist.endIndex)?.lowerBound else { return nil}
        let interpret = String(snippetPlaylist[titleRange..<titleRangeEnd])
        
        // find song">
        guard let songRange = snippetPlaylist.range(of: "song\">")?.upperBound,
            let songRangeEnd = snippetPlaylist.range(of: "</span>", range: songRange..<snippetPlaylist.endIndex)?.lowerBound else { return nil}
        let song = String(snippetPlaylist[songRange..<songRangeEnd])
        
        return SlovakSong(timestamp: programmeTime, interpret: interpret, track: song)
    }
    
}
