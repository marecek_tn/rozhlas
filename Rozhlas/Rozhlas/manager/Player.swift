//
//  Player.swift
//  Rozhlas
//
//  Created by Marek on 01/02/2020.
//  Copyright © 2020 Marek Chrenko. All rights reserved.
//

import Foundation
import AVFoundation

class Player {
    
    private let stream = AVPlayer()
    
    func play(station: Station) {
        stream.pause()
        stream.replaceCurrentItem(with: AVPlayerItem.init(url: station.streamUrl))
        stream.play()
    }
    
    func pause() {
        stream.pause()
        stream.replaceCurrentItem(with: nil)
    }
    
    var isPlaying : Bool  {
        return stream.rate > 0.0
    }
}
