//
//  Menubar.swift
//  Rozhlas
//
//  Created by Marek on 31/01/2020.
//  Copyright © 2020 Marek. All rights reserved.
//

import Foundation
import Cocoa
import Combine
import ServiceManagement

class Menubar: NSObject, NSMenuDelegate, NSMenuItemValidation {
    
    public static let RUN_AFTER_LOGIN_TAG = 54
    private static let RUN_AFTER_LOGIN_KEY = "runAfterLogin"
    private static let PLAY_SYMBOL = "▶  "
    
    private var statusItem: NSStatusItem?
    private let statusButton: NSStatusBarButton?
    weak var delegate: StationSelectDelegateProtocol?
    private var currentSongCancellable: AnyCancellable?
    private var streamStartCancellable: AnyCancellable?
    private var streamEndCancellable: AnyCancellable?
    
    init(title: String) {
        self.statusItem = NSStatusBar.system.statusItem(withLength: -1)
        self.statusButton = self.statusItem?.button
        super.init()
        let statusIcon = NSImage.init(named: "er")
        self.statusItem?.button?.image = statusIcon
        self.statusItem?.menu = createMenu()
        self.statusItem?.menu?.delegate = self
        
        
        // setup listener for current song
        self.currentSongCancellable = NotificationCenter.Publisher(center: .default, name: .currentSong, object: nil)
            .sink(receiveValue: { [weak self] notification in
                if let song = notification.object as? Song? {
                    self?.setCurrentSong(song: song)
                }
            })
        
        self.streamStartCancellable = NotificationCenter.Publisher(center: .default, name: .streamStart, object: nil)
            .sink(receiveValue: { [weak self] notification in
                if let station = notification.object as? Station {
                    self?.statusItem?.menu?.items.map { item in
                        if item.title == station.name {
                            item.title = "\(Menubar.PLAY_SYMBOL)\(station.name)"
                        }
                    }
                }
            })
        
        self.streamEndCancellable = NotificationCenter.Publisher(center: .default, name: .streamEnd, object: nil)
            .sink(receiveValue: { [weak self] _  in
                self?.statusItem?.menu?.items.map { item in
                    if item.title.starts(with: Menubar.PLAY_SYMBOL) {
                        let startIndex = item.title.index(item.title.startIndex, offsetBy: Menubar.PLAY_SYMBOL.count)
                        item.title = String(item.title[startIndex...])
                    }
                }
            })
    }
    
    deinit {
        currentSongCancellable?.cancel()
    }
    
    @objc public func doPlay(_ sender: Any) {
        guard let item = sender as? StationMenuItem else { return }
        delegate?.toggle(station: item.station)
    }
    
    @objc public func doCopySongToClipboard(_ sender: Any) {
        let title = (sender as! NSMenuItem).title
        
        NSPasteboard.general.declareTypes([NSPasteboard.PasteboardType.string], owner: nil)
        NSPasteboard.general.setString(title, forType: NSPasteboard.PasteboardType.string)
        
        let notification = NSUserNotification()
        notification.identifier = title
        notification.title = NSLocalizedString("app.name", comment: "application name")
        notification.subtitle = NSLocalizedString("track.copied.clipboard", comment: "Track has been copied to clipboard")
        notification.informativeText = title
        NSUserNotificationCenter.default.deliver(notification)
    }
    
    @objc public func toggleRunAfterLogin() {
        let runAfterLogin = !UserDefaults.standard.bool(forKey: Menubar.RUN_AFTER_LOGIN_KEY)
        SMLoginItemSetEnabled(ROCKET_LAUNCHER_BUNDLE as CFString, runAfterLogin)
        UserDefaults.standard.set(runAfterLogin, forKey: Menubar.RUN_AFTER_LOGIN_KEY)
    }
    
    @objc public func doQuit() {
        NSApp.terminate(nil)
    }
    
    @objc func validateMenuItem(_ menuItem: NSMenuItem) -> Bool {
        if menuItem.tag == Menubar.RUN_AFTER_LOGIN_TAG {
            menuItem.state = UserDefaults.standard.bool(forKey: Menubar.RUN_AFTER_LOGIN_KEY) ? .on : .off
        }
        return true
    }
}


private extension Menubar {
    private func setCurrentSong(song: Song?) {
        
        let statusItem = self.statusItem?.menu?.item(at: 0)
        guard let song = song else {
            DispatchQueue.main.async {
                statusItem?.title = NSLocalizedString("playlist.not.available", comment: "Playlist not available")
                statusItem?.action = nil
                statusItem?.target = nil
            }
            return
        }
        
        DispatchQueue.main.async {
            statusItem?.title = "\(song.interpret) - \(song.track)"
            statusItem?.action = #selector(self.doCopySongToClipboard(_:))
            statusItem?.target = self
        }
    }
    
        
    func createMenu(czechStations: [Station] = CZECH_STATIONS, slovakStations: [Station] = SLOVAK_STATIONS) -> NSMenu {
        let menu = NSMenu(title: "Rozhlas")
        
        menu.addItem(withTitle: NSLocalizedString("playlist.not.available", comment: "Playlist not available"),
                     action: nil,
                     keyEquivalent: "")
        
        menu.addItem(NSMenuItem.separator())
        
        for station in czechStations {
            let item = StationMenuItem(station: station, action: #selector(doPlay), keyEquivalent: "")
            item.target = self
            menu.addItem(item)
        }
        
        menu.addItem(NSMenuItem.separator())
        
        for station in slovakStations {
            let item = StationMenuItem(station: station, action: #selector(doPlay), keyEquivalent: "")
            item.target = self
            menu.addItem(item)
        }
        
        menu.addItem(NSMenuItem.separator())
        
        let runAfterLoginItem = NSMenuItem(title: NSLocalizedString("run.after.login", comment: "Run after login"), action: #selector(toggleRunAfterLogin), keyEquivalent: "")
        runAfterLoginItem.tag = Menubar.RUN_AFTER_LOGIN_TAG
        runAfterLoginItem.target = self
        menu.addItem(runAfterLoginItem)
        
        menu.addItem(withTitle: NSLocalizedString("quit", comment: "Quit plz"), action: #selector(doQuit), keyEquivalent: "")
            .target = self
            
            
        return menu
    }

}
