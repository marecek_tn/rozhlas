//
//  StationMenuItem.swift
//  Rozhlas
//
//  Created by Marek on 03/02/2020.
//  Copyright © 2020 Marek Chrenko. All rights reserved.
//

import Foundation
import Cocoa

class StationMenuItem: NSMenuItem {

    let station: Station

    init(station: Station, action selector: Selector?, keyEquivalent charCode: String) {
        self.station = station
        super.init(title: station.name, action: selector, keyEquivalent: charCode)
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
