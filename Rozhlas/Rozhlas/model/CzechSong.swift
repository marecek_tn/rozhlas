//
//  CzechSong.swift
//  Rozhlas
//
//  Created by Marek on 01/02/2020.
//  Copyright © 2020 Marek Chrenko. All rights reserved.
//

import Foundation

class CzechSong: Song, Codable {
    
    var timestamp: String
    var interpret: String
    var track: String
    
    private enum CodingKeys : String, CodingKey {
        case timestamp = "since", interpret, track
    }
}
