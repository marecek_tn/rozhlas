//
//  Station.swift
//  Rozhlas
//
//  Created by Marek on 01/02/2020.
//  Copyright © 2020 Marek Chrenko. All rights reserved.
//

import Foundation

struct Station: Equatable, Hashable {
    let id: String
    let name: String
    let streamUrl: URL
    let playlistUrl: URL?
    
    init(id: String, name: String, streamUrl: String, playlistUrl: String? = nil) {
        self.id = id
        self.name = name
        self.streamUrl = URL(string: streamUrl)!
        self.playlistUrl = playlistUrl != nil ? URL(string: playlistUrl!) : nil
    }
}
