//
//  SlovakSong.swift
//  Rozhlas
//
//  Created by Marek on 01/02/2020.
//  Copyright © 2020 Marek Chrenko. All rights reserved.
//

import Foundation

class SlovakSong: Song {
    
    var timestamp: String
    var interpret: String
    var track: String
    
    init(timestamp: String, interpret: String, track: String) {
        self.timestamp = timestamp
        self.interpret = interpret
        self.track = track
    }
    
}
