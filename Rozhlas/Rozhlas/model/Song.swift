//
//  Song.swift
//  Rozhlas
//
//  Created by Marek on 01/02/2020.
//  Copyright © 2020 Marek Chrenko. All rights reserved.
//

import Foundation

protocol Song: AnyObject {
    
    var timestamp: String { get }
    var interpret: String { get }
    var track: String { get }
    
}
