//
//  CzechPlaylist.swift
//  Rozhlas
//
//  Created by Marek on 01/02/2020.
//  Copyright © 2020 Marek Chrenko. All rights reserved.
//

import Foundation

struct CzechPlaylist: Codable {
    let timestamp: String
    let playlist: [CzechSong]
    
    private enum CodingKeys : String, CodingKey {
        case timestamp, playlist = "data"
    }
}
