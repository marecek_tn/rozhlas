//
//  Environment.swift
//  Rozhlas
//
//  Created by Marek on 01/02/2020.
//  Copyright © 2020 Marek Chrenko. All rights reserved.
//

import Foundation

let ROCKET_LAUNCHER_BUNDLE = "co.chren.macos.rozhlas.RocketLauncher"

let CZECH_STATIONS = [
    Station(id: "radiozurnal",
            name: "Rádiožurnál",
            streamUrl:"http://icecast7.play.cz/cro1-128.mp3",
            playlistUrl: "https://api.rozhlas.cz/data/v2/playlist/day/radiozurnal.json"),
    
    Station(id: "dvojka",
            name: "Dvojka",
            streamUrl:"http://icecast6.play.cz/cro2-128.mp3",
            playlistUrl: "https://api.rozhlas.cz/data/v2/playlist/day/dvojka.json"),
    
    Station(id: "vltava",
            name: "Vltava",
            streamUrl:"http://icecast5.play.cz/cro3-128.mp3"),
    
    Station(id: "plus",
            name: "Plus",
            streamUrl:"http://icecast1.play.cz/croplus128.mp3"),
    
    Station(id: "wave",
            name: "Wave",
            streamUrl:"http://icecast6.play.cz/crowave-128.mp3",
            playlistUrl: "https://api.rozhlas.cz/data/v2/playlist/day/radiowave.json"),
    
    Station(id: "d-dur",
            name: "D-Dur",
            streamUrl:"http://icecast5.play.cz/croddur-128.mp3"),
    
    Station(id: "jazz",
            name: "Jazz",
            streamUrl:"http://icecast1.play.cz/crojazz128.mp3",
            playlistUrl: "https://api.rozhlas.cz/data/v2/playlist/day/jazz.json"),
]

let SLOVAK_STATIONS = [
    Station(id: "sro",
            name: "Slovensko",
            streamUrl:"https://icecast.stv.livebox.sk/slovensko_128.mp3",
            playlistUrl: "https://slovensko.rtvs.sk/player"),
    
    Station(id: "devin",
            name: "Devín",
            streamUrl:"https://icecast.stv.livebox.sk/devin_128.mp3",
            playlistUrl: "https://devin.rtvs.sk/player"),
    
    Station(id: "_fm",
            name: "Rádio_FM",
            streamUrl:"https://icecast.stv.livebox.sk/fm_128.mp3",
            playlistUrl: "https://fm.rtvs.sk/player"),
    
    Station(id: "patria",
            name: "Patria",
            streamUrl:"https://icecast.stv.livebox.sk/patria_128.mp3",
            playlistUrl: "https://patria.rtvs.sk/player"),
    
    Station(id: "slovakia-int",
            name: "Slovakia International",
            streamUrl:"https://icecast.stv.livebox.sk/rsi_128.mp3",
            playlistUrl: "https://rsi.rtvs.sk/player"),
    
    Station(id: "pyramida",
            name: "Pyramída",
            streamUrl:"https://icecast.stv.livebox.sk/pyramida_128.mp3",
            playlistUrl: "https://pyramida.rtvs.sk/player"),
    
    Station(id: "litera",
            name: "Litera",
            streamUrl:"https://icecast.stv.livebox.sk/litera_128.mp3",
            playlistUrl: "https://litera.rtvs.sk/player")
]

