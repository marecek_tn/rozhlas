//
//  StationSelectDelegateProtocol.swift
//  Rozhlas
//
//  Created by Marek on 03/02/2020.
//  Copyright © 2020 Marek Chrenko. All rights reserved.
//

import Foundation

protocol StationSelectDelegateProtocol: AnyObject {
    func toggle(station: Station)
    func toggle()
}
