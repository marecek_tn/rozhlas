//
//  AppDelegate.swift
//  Rozhlas
//
//  Created by Marek on 31/01/2020.
//  Copyright © 2020 Marek Chrenko. All rights reserved.
//

import Cocoa
import SwiftUI
import MediaPlayer

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    var window: NSWindow!
    let menubar = Menubar(title: NSLocalizedString("app.name", comment: ""))
    let stationManager = StationManager()

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        menubar.delegate = self
        NSUserNotificationCenter.default.delegate = self
        
        // setup remote command for notification purposes
        let commandCenter = MPRemoteCommandCenter.shared()
        
        commandCenter.playCommand.isEnabled = false
        commandCenter.pauseCommand.isEnabled = false
        commandCenter.stopCommand.isEnabled = false
        commandCenter.nextTrackCommand.isEnabled = false
        commandCenter.previousTrackCommand.isEnabled = false
        
        commandCenter.togglePlayPauseCommand.addTarget() { [unowned self] event in
            self.toggle()
            return .success
        }
        
    }
}

extension AppDelegate: StationSelectDelegateProtocol {
    
    func toggle(station: Station) {
        stationManager.toggle(station: station)
    }
    
    func toggle() {
        stationManager.toggle()
    }
}

extension AppDelegate: NSUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: NSUserNotificationCenter, shouldPresent notification: NSUserNotification) -> Bool {
        return true
    }
}
